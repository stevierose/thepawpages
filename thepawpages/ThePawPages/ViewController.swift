//
//  ViewController.swift
//  ThePawPages
//
//  Created by Steven Roseman on 3/15/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit
import Parse
import Alamofire
import AlamofireImage



class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
//mongodb://<dbuser>:<dbpassword>@ds135690.mlab.com:35690/crosscode-app
    @IBOutlet var tableView: UITableView!
    var categories = [PFObject]()
    var images = [String]()
    var sections = [String]()
    var imageObjects = [PFObject]()
    var adImg:PFObject? = nil
    var categoryName:String!
    var categoryImg:PFFile!
    var counter:Int!
    var myCat:PFObject!
    let cellSpacingHeight: CGFloat = 10
    
    @IBOutlet var signoutLabelButton: UIBarButtonItem!
  
    
    @IBOutlet var activityIndicatorView: UIView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!

    


    @IBOutlet var adImages: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        


        let name = PFUser.current()

        if let currentName = name{
          //hide signout button
            print(currentName)
            
        } else {
            print("signout button hidden")
            signoutLabelButton.title = ""
        }
        if isReachable(){
            print("connection available")
        } else {
           addWifi()
        }
        checkIfLoginSkipped()
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()

        navBar()
        
        changingPhoto()

        tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.showsVerticalScrollIndicator = false
       
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return categories.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! PetCell
        let listData:PFObject = categories[indexPath.section]
        cell.petCategory.text = listData.object(forKey: "category_name") as! String?
        let img = listData.object(forKey: "category_img")as? PFFile
        cell.petImg.loadImageUsingCacheWithUrlString(urlString: img!)
        
        cell.backgroundColor = UIColor.white
        cell.layer.borderColor = UIColor.black.cgColor
        cell.layer.borderWidth = 1
//        cell.layer.cornerRadius = 8
//        cell.clipsToBounds = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         tableView.deselectRow(at: indexPath, animated: true)
        
        if counter == .none{
            counter = 0
        }
       
        myCat = categories[indexPath.section]
        let adoptionCat = myCat.object(forKey: "category_name") as? String
        
        let merchandiseCat = myCat.object(forKey: "category_name") as? String
        
        if merchandiseCat == "Merchandiser"{
            let controller = self.storyboard!.instantiateViewController(withIdentifier: "MerchandiserView") as! MerchandiserView
            
            
            let controllerNav = UINavigationController(rootViewController: controller)
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = controllerNav
            
        } else if adoptionCat == "Adoption"{
                    let controller = self.storyboard!.instantiateViewController(withIdentifier: "DogAdoptonView") as! DogAdoptonView
            
            
                    let controllerNav = UINavigationController(rootViewController: controller)
            
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
                    
                    
                    appDelegate.window?.rootViewController = controllerNav
        } else {
//                    let controller = self.storyboard!.instantiateViewController(withIdentifier: "CategoryController") as! CategoryController
//                    controller.counter = self.counter
//            
//                    let controllerNav = UINavigationController(rootViewController: controller)
//            
//                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            
//                    controller.catChosen = [myCat]
//                    
//                    appDelegate.window?.rootViewController = controllerNav
            
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc: UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabBarController") as! UITabBarController
                vc.selectedIndex = 1
            //save category name using user default
            let name = myCat.object(forKey: "category_name")
            let users = UserDefaults.standard
            users.setValue(name, forKey: "UserKey")
//            if let vcs = vc.viewControllers,
//                let nc = vcs.first as? UINavigationController,
//                let pendingOverVC = nc.topViewController as? CategoryController {
//                pendingOverVC.catChosen = [myCat]
//                
//            }
            self.present(vc, animated: true, completion: nil)

        }

        
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.groupTableViewBackground
        return headerView
    }
    
    func changingPhoto(){
        
        let query = PFQuery(className: "Promotions")
        query.findObjectsInBackground { (success, error) in
            
            if (success != nil) {
                //change image
                
                self.imageObjects = success!
                let random = arc4random_uniform(UInt32(self.imageObjects.count)) 
                //self.adImg = self.imageObjects.objectAtIndex(0) as! PFObject
                let randomInt = Int(random)
                let currAd = self.imageObjects[randomInt]["image"] as! String
                
                Alamofire.request(currAd).responseImage(completionHandler: { (response) in
                    if let image = response.result.value{
                        self.adImages.image = image
                    }
                })
//                
//                let imgFileURL:NSURL = NSURL(string: currAd.url!)!
//                let imageData:NSData = (NSData(contentsOf: imgFileURL as URL))!
//                self.adImages.image = UIImage(data: imageData as Data)
                
                //add category data and show
                self.tableView.reloadData()
                 self.queryCategories()
            } else {
                print(error?.localizedDescription as Any)
            }
        }
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "GoToTabBar"{
//            
//         
//          
//                    let controller = self.storyboard!.instantiateViewController(withIdentifier: "CategoryController") as! CategoryController
//                    controller.counter = self.counter
//            
//                    let controllerNav = UINavigationController(rootViewController: controller)
//            
//                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            
//                    controller.catChosen = [myCat]
//                    
//                    appDelegate.window?.rootViewController = controllerNav
//        }
//    }
//    
    
    @IBAction func signOutButtonTapped(_ sender: Any) {
        PFUser.logOutInBackground { (error) in
            if (error != nil){
                print(error?.localizedDescription as Any)
            } else {
                _ = PFUser.current()
                
                                let controller = self.storyboard!.instantiateViewController(withIdentifier: "LoginView") as! LoginView
                                let controllerNav = UINavigationController(rootViewController: controller)
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                
                                appDelegate.window?.rootViewController = controllerNav
            }
        }
        
        

    }
    
    func queryCategories(){
        let query = PFQuery(className: "Categories")
        query.findObjectsInBackground { (objects, error) in
            if objects != nil{
                self.activityIndicatorView.isHidden = true
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                self.categories = objects!
                self.tableView.reloadData()
            }
        }
    }
    

    
}

