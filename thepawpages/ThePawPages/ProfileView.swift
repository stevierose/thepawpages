//
//  ProfileView.swift
//  ThePawPages
//
//  Created by Steven Roseman on 3/29/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit
import Parse


class ProfileView: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {


    @IBOutlet var imgView: UIImageView!
    var img:UIImage? = nil
    var videoFilePath:String!
    let imagePicker:UIImagePickerController = UIImagePickerController()
    var isImgTaken = false
    var currentUser:String!
    
    @IBOutlet var addPhotoLabel: UILabel!
    @IBOutlet var welcomePawPagesLabel: UILabel!
    
    @IBOutlet var newUserImg: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isReachable(){
            print("connection available")
        } else {
            addWifi()
        }

        navBar()
        let user = PFUser.current()?.username
        
        
        welcomePawPagesLabel.text = "Welcome to Paw Pages, " + user! + ""
        

        imgTapGesture()
        
        self.img = UIImage()
        self.imagePicker.delegate = self
        self.imagePicker.allowsEditing = false
       
    }

   
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
                let theInfo:NSDictionary = info as NSDictionary
        
                img = theInfo.object(forKey: UIImagePickerControllerOriginalImage) as? UIImage
        
                UIImageWriteToSavedPhotosAlbum(img!, nil, nil, nil)
        
                print(img!)
                newUserImg.image = img
        
                self.dismiss(animated: true, completion: nil)
        
        isImgTaken = true
    }
    
    
//    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
//        
//        let theInfo:NSDictionary = info as NSDictionary
//        
//        img = theInfo.object(forKey: UIImagePickerControllerOriginalImage) as? UIImage
//        
//        UIImageWriteToSavedPhotosAlbum(img!, nil, nil, nil)
//        
//        print(img)
//        imgView.image = img
//        
//        self.dismiss(animated: true, completion: nil)
//        
//    }
    
    func resizeImage(image:UIImage, width:CGFloat, height:CGFloat)->UIImage{
        
        let newSize = CGSize(width: width, height: height)
        let newRectangle = CGRect(x: 0, y: 0, width: width, height: height)
        UIGraphicsBeginImageContext(newSize)
        image.draw(in: newRectangle)
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return resizedImage!
        
    }

    @IBAction func backButtonTapped(_ sender: Any) {
        
//        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//        let vc: UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
//        self.present(vc, animated: true, completion: nil)
        
        if PFUser.current() != nil {
            PFUser.current()?.deleteInBackground(block: { (deleteSuccessful, error) -> Void in
                print("success = \(deleteSuccessful)")
                PFUser.logOut()
            })
        }
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "SignUpView") as! SignUpView
        let controllerNav = UINavigationController(rootViewController: controller)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
      
        appDelegate.window?.rootViewController = controllerNav
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        self.dismiss(animated: false, completion: nil)
//        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//        let vc: UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
//        self.present(vc, animated: true, completion: nil)
    }
   
    func reset() {
        img = nil
        
        videoFilePath = nil
    }
    
    func handleImgViewTap(_sender: UITapGestureRecognizer){
    
        addPhotoLabel.isHidden = true
        
        let imgAlert = UIAlertController(title: "", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let takePhoto = UIAlertAction(title: "Take Photo", style: UIAlertActionStyle.default) { (action) in
            //take new photo
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
                self.imagePicker.sourceType = .camera
                
                self.imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: self.imagePicker.sourceType)!
                
                self.present(self.imagePicker, animated: false, completion: nil)
            }
        }
        
        let choosePhoto = UIAlertAction(title: "Choose Photo", style: UIAlertActionStyle.default) { (action) in
            
            self.imagePicker.sourceType = .photoLibrary
            
            self.imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: self.imagePicker.sourceType)!
            
            self.present(self.imagePicker, animated: false, completion: nil)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { (action) -> Void in
            self.addPhotoLabel.isHidden = false
        })

        
        
        imgAlert.addAction(takePhoto)
        imgAlert.addAction(choosePhoto)
        imgAlert.addAction(cancelAction)
        
        self.present(imgAlert, animated: true, completion: nil)
    }
    
    func imgTapGesture(){
         let imgTap = UITapGestureRecognizer(target: self, action: #selector(handleImgViewTap(_sender:)))
        
        addPhotoLabel.addGestureRecognizer(imgTap)
        addPhotoLabel.isUserInteractionEnabled = true
        view.addSubview(addPhotoLabel)
        
    }
    
    @IBAction func signUpButtonTapped(_ sender: Any) {
        
        if isImgTaken == false{
            // alert image not added
            alertImgNotAdded()
        } else {
            submitPhotoinUserAcct()
        }
    }
    
    func submitPhotoinUserAcct(){
        let userImage = resizeImage(image: newUserImg.image!, width: 250.0, height: 194.0)
        let file = PFFile(data: UIImageJPEGRepresentation(userImage, 1.0)!)
        print(file!)
        if let currentUser = PFUser.current(){
            currentUser["profile_picture"] = file
            //set other fields the same way....
            currentUser.saveInBackground(block: { (success, error) in
                if success{
                    print("image and user data added")
                    self.initialUserViewSaved()
                    
//                    let controller = self.storyboard!.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//                    let controllerNav = UINavigationController(rootViewController: controller)
//                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                    
//                    appDelegate.window?.rootViewController = controllerNav
                    
                    
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                    let vc: UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabBarController") as! UITabBarController
                    
                    self.present(vc, animated: true, completion: nil)
                    
                } else {
                    print(error?.localizedDescription as Any)
                }
            })
        }
       
    }
}
