//
//  MerchandiserDetailView.swift
//  ThePawPages
//
//  Created by Steven Roseman on 5/9/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit
import Parse

class MerchandiserDetailView: UIViewController {

    @IBOutlet var emailAddress: UILabel!
    @IBOutlet var companyName: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var phoneNum: UILabel!    
    @IBOutlet var services: UITextView!
    var listData = [PFObject]()
    override func viewDidLoad() {
        super.viewDidLoad()

        navBar()
        for merchandiserInfo in listData{
            let name = merchandiserInfo["name"] as! String
            companyName.text = name
            let phoneNumber = merchandiserInfo["phoneNum"] as! String
            phoneNum.text = phoneNumber
            let allServices = merchandiserInfo["services"] as! String
            services.text = allServices
        }
    }

   
}
