//
//  PetCell.swift
//  ThePawPages
//
//  Created by Steven Roseman on 3/15/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit

class PetCell: UITableViewCell {

    @IBOutlet var petImg: UIImageView!
    @IBOutlet var petCategory: UILabel!
   
}
