//
//  SignUpView.swift
//  ThePawPages
//
//  Created by Steven Roseman on 4/8/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit
import Parse

class SignUpView: UIViewController, UITextFieldDelegate {

    @IBOutlet var emailTextfield: UITextField!
    @IBOutlet var passwordTextfield: UITextField!
    @IBOutlet var usernameTextfield: UITextField!
    var newUser:String!
    var newEmail:String!
    var newPassword:String!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()

        if isReachable(){
            print("connection available")
        } else {
            addWifi()
        }

        activityIndicator.isHidden = true
        
        navBar()
        hideKeyboardWhenTappedAround()
        passwordTextfield.delegate = self
        
    }


    @IBAction func xButtonTapped(_ sender: Any) {
        
                let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                let vc: UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "FirstTimeLoginView") as! FirstTimeLoginView
                self.present(vc, animated: true, completion: nil)
        
//        let controller = self.storyboard!.instantiateViewController(withIdentifier: "LoginView") as! LoginView
//        let controllerNav = UINavigationController(rootViewController: controller)
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        
//        appDelegate.window?.rootViewController = controllerNav
    }
    
    @IBAction func signUpbuttonTapped(_ sender: Any) {

        signUpData()
        self.activityIndicator.startAnimating()
        self.activityIndicator.isHidden = false
    }

    func signUpUser(username:String, email:String, password:String){
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
        
        let user:PFUser = PFUser()
        user.email = email
        user.password = password
        user.username = username
        
        user.signUpInBackground { (success, error) in
            if success{
               
               
              
                
                let controller = self.storyboard!.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
                let controllerNav = UINavigationController(rootViewController: controller)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                controller.currentUser = username
                appDelegate.window?.rootViewController = controllerNav
            } else {
                let alert = UIAlertController(title: "Oops!", message: error?.localizedDescription, preferredStyle: UIAlertControllerStyle.actionSheet)
                let alertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in

                    self.emailTextfield.text = ""
                    self.usernameTextfield.text = ""
                    self.passwordTextfield.text = ""
                })
                alert.addAction(alertAction)
                self.present(alert, animated: true, completion: nil)
               print(error?.localizedDescription as Any)
            }
        }
    }
    
    func isValidPassword(candidate:String!)->Bool{
        
        let passwordRegex = "(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{6,25}"
        
        let passwordTest = NSPredicate(format:"SELF MATCHES %@",passwordRegex)
        let result = passwordTest.evaluate(with: candidate)
        
        return result
    }
    
    func isValidInput(Input:String) -> Bool {
        let RegEx = "\\A\\w{4,15}\\z"
        let Test = NSPredicate(format:"SELF MATCHES %@", RegEx)
        return Test.evaluate(with: Input)
    }
    
    func validate(YourEMailAddress: String) -> Bool {
        let REGEX: String
        REGEX = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluate(with: YourEMailAddress)
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        
        signUpData()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        passwordTextfield.resignFirstResponder()
        
        signUpData()
        return true
    }
    
    func signUpData(){
        
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        
        newPassword = passwordTextfield.text
        newEmail = emailTextfield.text
        newUser = usernameTextfield.text
        
        if isValidInput(Input: newUser){
            print("Valid user")
            if newUser == ""{
                //alert no username added
                alertNoUsernameAdded()
            }
        } else {
            alertUsernameError()
            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
            
            self.emailTextfield.text = ""
            self.usernameTextfield.text = ""
            self.passwordTextfield.text = ""
        }
    
        if isValidPassword(candidate: newPassword){
            if newPassword == ""{
                //alert no password entered
                alertNoPasswordAdded()
            }
        } else {
            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
            
            self.emailTextfield.text = ""
            self.usernameTextfield.text = ""
            self.passwordTextfield.text = ""
            
            alertPasswordError()
            return
        }
        
        
        if validate(YourEMailAddress: newEmail){
            if newEmail == ""{
                //alert no email address added
                alertNoEmailAddressAdded()
            }
        } else {
            //alert email not valid
            self.emailTextfield.text = ""
            self.usernameTextfield.text = ""
            self.passwordTextfield.text = ""
            alertEmailError()
            activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
            return
        }
        
        if newPassword.characters.count <= 5 || newPassword.characters.count > 15 {
            //alert not enough characters
            alertPasswordError()
            activityIndicator.isHidden = true
            
            return
        }
        
        if newUser.characters.count <= 3 || newUser.characters.count > 15 {
            //alert not enough characters
            alertUsernameError()
            activityIndicator.isHidden = true
            
            return
        }
        
        signUpUser(username: newUser, email: newEmail, password: newPassword)

    }
    
}
