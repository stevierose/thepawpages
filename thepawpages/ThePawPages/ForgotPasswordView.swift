//
//  ForgotPasswordView.swift
//  ThePawPages
//
//  Created by Steven Roseman on 4/8/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit
import Parse

class ForgotPasswordView: UIViewController {

    @IBOutlet var emailTextfield: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        if isReachable(){
            print("connection available")
        } else {
            addWifi()
        }

        hideKeyboardWhenTappedAround()
        
        navBar()
    }



    @IBAction func submitButtonTapped(_ sender: Any) {
        if !validate(YourEMailAddress: emailTextfield.text!){
            //alert
            let alertController = UIAlertController(title: "Valid email is required", message: "Please re-enter", preferredStyle: UIAlertControllerStyle.alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                
            })
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
            return
        }
        
        PFUser.requestPasswordResetForEmail(inBackground: emailTextfield.text!) { (success, error) in
            if success{
                
            }else {
                print(error!)
            }
        }
        
    }
    @IBAction func backButtonTapped(_ sender: Any) {
//        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//        let vc: UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginView") as! LoginView
//        self.present(vc, animated: true, completion: nil)

        
            let controller = self.storyboard!.instantiateViewController(withIdentifier: "LoginView") as! LoginView
            let controllerNav = UINavigationController(rootViewController: controller)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = controllerNav
        
    }
    
    func validate(YourEMailAddress: String) -> Bool {
        let REGEX: String
        REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluate(with: YourEMailAddress)
    }

}
