//
//  FirstTimeLoginView.swift
//  ThePawPages
//
//  Created by Steven Roseman on 4/17/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit
import Parse
import CoreData
import ParseFacebookUtilsV4
import FBSDKCoreKit

class FirstTimeLoginView: UIViewController {

//    var initialViewSkip: [NSManagedObject] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        if isReachable(){
            print("connection available")
        } else {
            addWifi()
        }

       
    }


    @IBAction func loginButtonTapped(_ sender: Any) {
        
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "LoginView") as! LoginView
        let controllerNav = UINavigationController(rootViewController: controller)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = controllerNav
        
    }
    @IBAction func skipButtonTapped(_ sender: Any) {
        initialUserViewSaved()
//        let controller = self.storyboard!.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//        let controllerNav = UINavigationController(rootViewController: controller)
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        
//        appDelegate.window?.rootViewController = controllerNav
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc: UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabBarController") as! UITabBarController
        
        self.present(vc, animated: true, completion: nil)
    }
 
    @IBAction func newUserButtonTapped(_ sender: Any) {
        
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "SignUpView") as! SignUpView
        let controllerNav = UINavigationController(rootViewController: controller)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = controllerNav
        
    }
    
    @IBAction func loginFacebookButtonTapped(_ sender: Any) {
        
        PFFacebookUtils.logInInBackground(withReadPermissions: ["public_profile", "email"]) { (user:PFUser?, error) in
            
            
            if let user = user {
                if user.isNew {
                   self.initialUserViewSaved()
                    print("User signed up and logged in through Facebook!")
//                    let controller = self.storyboard!.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//                    let controllerNav = UINavigationController(rootViewController: controller)
//                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                    
//                    appDelegate.window?.rootViewController = controllerNav
                    
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                    let vc: UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabBarController") as! UITabBarController
                    
                    self.present(vc, animated: true, completion: nil)
                    
                } else {
                    self.initialUserViewSaved()
//                    let controller = self.storyboard!.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//                    let controllerNav = UINavigationController(rootViewController: controller)
//                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                    
//                    appDelegate.window?.rootViewController = controllerNav
                    
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                    let vc: UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabBarController") as! UITabBarController
                    
                    self.present(vc, animated: true, completion: nil)
                    print("User logged in through Facebook!")
                }
            } else {
                
                
                print("Uh oh. The user cancelled the Facebook login.")
            }
            
            
           
        }
    }
    
        
    
//    func checkIfLoginSkipped(){
//        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
//            return
//        }
//        
//        let managedContext = appDelegate.persistentContainer.viewContext
//        
//        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Login")
//        do{
//            initialViewSkip = try managedContext.fetch(fetchRequest)
//
//            print(initialViewSkip.count)
//            if initialViewSkip.count >= 1{
//                //dont do anything
//            } else {
//               //send to initialView
//            }
////            for peeps in initialViewSkip{
////                let skipped:String! = peeps.value(forKey: "skipped")as? String
////                print(skipped)
////                let controller = self.storyboard!.instantiateViewController(withIdentifier: "ViewController") as! ViewController
////                let controllerNav = UINavigationController(rootViewController: controller)
////                let appDelegate = UIApplication.shared.delegate as! AppDelegate
////                
////                appDelegate.window?.rootViewController = controllerNav
////
////                
////            }
//           
//        } catch let error as NSError{
//            
//            print(error)
//            
//        }
//    }
    @IBAction func button(_ sender: Any) {
        self.performSegue(withIdentifier: "GoToTabBar", sender: nil)
    }
    
 

    
    
}
