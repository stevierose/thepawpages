//
//  DogAdoptionDetailView.swift
//  ThePawPages
//
//  Created by Steven Roseman on 4/23/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import MessageUI

class DogAdoptionDetailView: UIViewController, MFMailComposeViewControllerDelegate {
    @IBOutlet var dogNameLabel: UILabel!
    @IBOutlet var dogSexLabel: UILabel!
    @IBOutlet var dogAgeLabel: UILabel!
    @IBOutlet var dogSizeLabel: UILabel!
    @IBOutlet var dogBreedLabel: UILabel!

    @IBOutlet var dogEmailLabel: UIButton!


    @IBOutlet var dogImg: UIImageView!
    @IBOutlet var collectionView: UICollectionView!
    var dogDescription:String!
    var listData = [AdoptablePups]()
    var email:String!
    @IBOutlet var descriptionText: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        if isReachable(){
            print("connection available")
        } else {
            addWifi()
        }

        navBar()
        for dogDescription in listData{
            let description = dogDescription.description
            descriptionText.text = description
            
            Alamofire.request(dogDescription.photos).responseImage(completionHandler: { (response) in
                if let image = response.result.value{
                    
                    self.dogImg.image = image
                    self.dogNameLabel.text = dogDescription.name
                    self.dogSexLabel.text = dogDescription.sex
                    self.dogAgeLabel.text = dogDescription.age
                    self.dogSizeLabel.text = dogDescription.size
                    self.dogBreedLabel.text = dogDescription.breed
                    //self.dogEmailLabel.text = dogDescription.email
                    self.email = dogDescription.email
                    self.dogEmailLabel.setTitle(dogDescription.email, for:.normal)
                }
            })

  
            
           
        }
//        collectionView.dataSource = self
//        collectionView.delegate = self
    }


//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 1
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return listData.count
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! DogDetailCollectionCell
//        for descript in listData{
//            print(descript.photos)
//            Alamofire.request(descript.photos).responseImage(completionHandler: { (response) in
//                if let image = response.result.value{
//            
//                    cell.pupImages.image = image
//                    
//                }
//            })
//            
//            
//            
//        }
//       
//        
//        
//        return cell
//    }
    
    @IBAction func dogEmailButton(_ sender: Any) {
        
        //send to email
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        // Configure the fields of the interface.
        composeVC.setToRecipients([email])
        composeVC.setSubject("Adoption")
//        composeVC.setMessageBody("Hello this is my message body!", isHTML: false)
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
//        deleteData()
        
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "DogAdoptonView") as! DogAdoptonView
        
        
        let controllerNav = UINavigationController(rootViewController: controller)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
//        controller.allDogs = listData
        
        appDelegate.window?.rootViewController = controllerNav

    }
    
//    func mailComposeController(controller: MFMailComposeViewController,
//                               didFinishWithResult result: MFMailComposeResult, error: NSError?) {
//        // Check the result or perform other tasks.
//        // Dismiss the mail compose view controller.
//        controller.dismiss(animated: true, completion: nil)
//    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
