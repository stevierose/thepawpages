//
//  LocationController.swift
//  ThePawPages
//
//  Created by Steven Roseman on 5/6/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit
import CoreLocation
class LocationController: UIViewController, CLLocationManagerDelegate {

    
    var locationManager: CLLocationManager = CLLocationManager()
    var startLocation: CLLocation!
    var location:CLLocation! = nil
    var distanceInMiles:Double!
    var latDbl:Double!
    var lngDbl:Double!
    var latestLocation:CLLocation!
    var doubleStr:String!

    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        
        
        
    }

    func locationMgr(){
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()

        startLocation = nil
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        
        if startLocation == nil{
            //startLocation = latestLocation as CLLocation
            latestLocation = locations.last
            startLocation = latestLocation
            locationManager.stopUpdatingLocation()
            
        }
        self.locationManager.stopUpdatingLocation()
    }

    func latestLocale(){
        if latestLocation == .none{
            self.locationManager.startUpdatingLocation()
        } else {
            
            let theLat = latestLocation.coordinate.latitude
            let theLng = latestLocation.coordinate.longitude
            let location = CLLocation(latitude: theLat, longitude: theLng)
            
            let location2 = CLLocation(latitude: latDbl, longitude: lngDbl)
            
            
            
            let distance = location.distance(from: location2)
            if distance <= 1609{
                //under 1 mile
            } else {
                //over 1 mile
                distanceInMiles = distance / 1609.344
                doubleStr = String(format: "%.1f", ceil(distanceInMiles*100)/100)
                
                locationManager.stopUpdatingLocation()
            }
            
            
            
//            if let distance = doubleStr{
//                
//                cell.distanceFromCompanyLbl.text = "\(distance)"
//                
//                
//            } else {
//                print("no distance")
//            }
            
            
        }
        
    }
}
