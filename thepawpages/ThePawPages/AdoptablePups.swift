//
//  AdoptablePups.swift
//  ThePawPages
//
//  Created by Steven Roseman on 4/22/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import Foundation

class AdoptablePups {
    
    var status:String!
    var email:String!
    var hasShots:String!
    var houseTrained:String!
    var age:String!
    var size:String!
    var photos:String!
    var breed:String!
    var name:String!
    var sex:String!
    var description:String!
    
}
