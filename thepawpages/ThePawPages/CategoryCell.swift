//
//  CategoryCell.swift
//  ThePawPages
//
//  Created by Steven Roseman on 3/22/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {

    @IBOutlet var companyImg: UIImageView!
    @IBOutlet var comanyNameLbl: UILabel!
    @IBOutlet var companyRating: CosmosView!
    @IBOutlet var distanceFromCompanyLbl: UILabel!
    @IBOutlet var companyAddress: UILabel!
    @IBOutlet var companyServices: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
   
    

}
