//
//  SIgnoutAndProfileController.swift
//  ThePawPages
//
//  Created by Steven Roseman on 4/11/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit
import Parse
class SIgnoutController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        PFUser.logOutInBackground { (error) in
            if (error != nil){
                print(error?.localizedDescription as Any)
            } else {
                _ = PFUser.current()
                
                let controller = self.storyboard!.instantiateViewController(withIdentifier: "LoginView") as! LoginView
                let controllerNav = UINavigationController(rootViewController: controller)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                
                appDelegate.window?.rootViewController = controllerNav
            }
        }

        // Do any additional setup after loading the view.
    }

    
}
