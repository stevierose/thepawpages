//
//  DetailUserReview.swift
//  ThePawPages
//
//  Created by Steven Roseman on 4/12/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit
import Parse
class DetailUserReview: UIViewController {

    @IBOutlet var ratingView: CosmosView!
    @IBOutlet var usersReview: UILabel!
    var userReviewObj = [PFObject]()
    var catChosen = [PFObject]()
    var dogObjectDetails = [PFObject]()
    var review:String!
    var rating:String!
    var latestReview:String!
    var latestRating:String!
    var companyName:String!
    var counter:Int!
    override func viewDidLoad() {
        super.viewDidLoad()

        if isReachable(){
            print("connection available")
        } else {
            addWifi()
        }

        reviewDetails()
        detailReviewCompanyNameNav()
    }


    @IBAction func backButtonTapped(_ sender: Any) {
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "UserPostedReviews") as! UserPostedReviews
        let controllerNav = UINavigationController(rootViewController: controller)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        controller.userReviewObj = self.userReviewObj
        controller.dogObjectDetails = self.dogObjectDetails
        controller.catChosen = self.catChosen
        controller.companyName = self.companyName
        controller.counter = self.counter
        appDelegate.window?.rootViewController = controllerNav
        
    }
   
    func reviewDetails(){
        ratingView.rating = Double(latestRating)!
        ratingView.settings.updateOnTouch = false
        usersReview.text = latestReview
    }
    
    func detailReviewCompanyNameNav(){
        if let nav = navigationController?.navigationBar{
            
            nav.backgroundColor = UIColor(red: 0.34, green: 0.50, blue: 0.41, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.34, green: 0.50, blue: 0.41, alpha: 1.0)
            
            nav.topItem?.title = self.companyName
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
            
            
        }

    }
}
