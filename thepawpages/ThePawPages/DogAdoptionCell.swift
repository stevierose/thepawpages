//
//  DogAdoptionCell.swift
//  ThePawPages
//
//  Created by Steven Roseman on 4/22/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit

class DogAdoptionCell: UITableViewCell {

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var breedLabel: UILabel!
    @IBOutlet var sexLabel: UILabel!
    @IBOutlet var ageLabel: UILabel!
    @IBOutlet var sizeLabel: UILabel!
    @IBOutlet var pupImg: UIImageView!

  
}
