//
//  DogLocation.swift
//  ThePawPages
//
//  Created by Steven Roseman on 3/22/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import Foundation

class DogLocation{
    
    var name:String!
    var address:String!
    var phoneNum:String!
    var lat:String!
    var lng:String!
    var rating:String!
    var hours:String!
    var website:String!
    
}
